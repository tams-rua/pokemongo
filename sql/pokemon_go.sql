-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  jeu. 04 fév. 2021 à 17:38
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pokemon_go`
--

-- --------------------------------------------------------

--
-- Structure de la table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
CREATE TABLE IF NOT EXISTS `abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_abilities_types` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `abilities`
--

INSERT INTO `abilities` (`id`, `name`, `image`, `type_id`) VALUES
(1, 'Riposte', NULL, 1),
(2, 'Frappe Atlas', NULL, 1),
(3, 'Double Pied', NULL, 1),
(4, 'Balayage', NULL, 1),
(5, 'Mawashi Geri	', NULL, 1),
(6, 'Sacrifice', NULL, 1),
(7, 'Pied Voltige', NULL, 1),
(8, 'Draco-Rage', NULL, 1),
(9, 'Repli', NULL, 1),
(10, 'Écume', NULL, 1),
(11, 'Claquoir', NULL, 1),
(12, 'Pistolet à O', NULL, 1),
(13, 'Bulles dO', NULL, 1),
(14, 'Spore', NULL, 1),
(15, 'Cage élcair', NULL, 1),
(16, 'Confusion', NULL, 1),
(17, 'Danse flemme', NULL, 1),
(18, 'Danse Fleur', NULL, 1),
(19, 'Cru-ailes', NULL, 1),
(20, 'Dard-Nuée', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `ability_pokemon`
--

DROP TABLE IF EXISTS `ability_pokemon`;
CREATE TABLE IF NOT EXISTS `ability_pokemon` (
  `pokemon_id` int(11) NOT NULL,
  `abilitie_id` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`abilitie_id`),
  KEY `fk_pokemons_has_abilities_abilities1_idx` (`abilitie_id`),
  KEY `fk_pokemons_has_abilities_pokemons1_idx` (`pokemon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ability_pokemon`
--

INSERT INTO `ability_pokemon` (`pokemon_id`, `abilitie_id`) VALUES
(9, 1),
(6, 2),
(4, 8),
(5, 8),
(6, 8),
(3, 9),
(10, 9),
(9, 11),
(7, 12),
(7, 13),
(8, 13),
(1, 14),
(2, 14),
(8, 14),
(2, 16),
(4, 17),
(5, 17),
(1, 18),
(3, 18),
(12, 19),
(10, 20),
(12, 20);

-- --------------------------------------------------------

--
-- Structure de la table `envs`
--

DROP TABLE IF EXISTS `envs`;
CREATE TABLE IF NOT EXISTS `envs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `envs`
--

INSERT INTO `envs` (`id`, `name`) VALUES
(1, 'plaine'),
(2, 'forêt'),
(3, 'mer'),
(4, 'Cavernes'),
(5, 'montagne'),
(6, 'sous terrain'),
(7, 'urbain'),
(8, 'rare');

-- --------------------------------------------------------

--
-- Structure de la table `env_pokemon`
--

DROP TABLE IF EXISTS `env_pokemon`;
CREATE TABLE IF NOT EXISTS `env_pokemon` (
  `pokemon_id` int(11) NOT NULL,
  `env_id` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`env_id`),
  KEY `fk_pokemons_has_envs_envs1_idx` (`env_id`),
  KEY `fk_pokemons_has_envs_pokemons1_idx` (`pokemon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `env_pokemon`
--

INSERT INTO `env_pokemon` (`pokemon_id`, `env_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(12, 1),
(2, 2),
(3, 2),
(10, 2),
(7, 3),
(8, 3),
(11, 3),
(5, 4),
(6, 4),
(1, 5),
(4, 5),
(5, 5),
(6, 5),
(4, 6),
(6, 6),
(9, 7);

-- --------------------------------------------------------

--
-- Structure de la table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caught_at` datetime DEFAULT NULL,
  `level` varchar(45) DEFAULT NULL,
  `height` varchar(45) DEFAULT NULL,
  `weight` varchar(45) DEFAULT NULL,
  `pokemon_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inventories_pokemons1_idx` (`pokemon_id`),
  KEY `fk_inventories_ users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inventories`
--

INSERT INTO `inventories` (`id`, `caught_at`, `level`, `height`, `weight`, `pokemon_id`, `users_id`) VALUES
(1, '2021-02-02 12:30:51', '40', '50cm', '30kg', 1, 3),
(2, '2021-01-02 12:32:42', '45', '70cm', '35kg', 7, 3),
(3, '2021-01-29 12:37:10', '20', '35cm', '20kg', 7, 3),
(4, '2020-12-16 13:40:51', '12', '22cm', '12kg', 6, 1),
(5, '2021-01-13 18:11:09', '7', NULL, NULL, 14, 2),
(6, '2021-01-13 18:11:09', '10', NULL, NULL, 16, 2),
(7, '2021-01-30 18:14:10', '50', '110cm', '19kg', 5, 9);

-- --------------------------------------------------------

--
-- Structure de la table `pokemons`
--

DROP TABLE IF EXISTS `pokemons`;
CREATE TABLE IF NOT EXISTS `pokemons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(198) DEFAULT NULL,
  `weight` varchar(45) DEFAULT NULL,
  `height` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `pokemon_evo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pokemons_pokemons1_idx` (`pokemon_evo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pokemons`
--

INSERT INTO `pokemons` (`id`, `name`, `description`, `weight`, `height`, `gender`, `image`, `pokemon_evo_id`) VALUES
(1, 'Bulbizarre', 'Il peut survivre plusieurs jours sans manger grâce aux nutriments contenus dans le bulbe sur son dos.', '10kg', '50cm', 'M', NULL, 2),
(2, 'Herbizarre', 'En emmagasinant de l\'énergie, son bulbe grossit. Un arôme en émane quand il s\'apprête à éclore', '15kg', '70cm', 'M', NULL, 3),
(3, 'Florizarre', 'Les pétales de sa fleur dorsale absorbent les rayons du soleil pour les convertir en énergie.', '20kg', '100cm', 'F', NULL, NULL),
(4, 'Salamèche', 'La flammèche au bout de sa queue émet un crépitement audible seulement dans les endroits calmes.', '50kg', '70cm', 'M', NULL, 5),
(5, 'Reptincel', 'Exalté quand il affront des adversaires puissants, ce Pokémon en vient parfois à cracher des flammes bleutées.', '70kg', '150cm', 'F', NULL, 6),
(6, 'Dracaufeu', 'Quand il crache son souffle brûlant, la flamme au bout de sa queue s\'embrase.', '100kg', '200cm', 'M', NULL, NULL),
(7, 'Carapuce', 'Caché sous les flots, il crache un jet d\'eau sur sa proie et se réfugie à l\'intérieur de sa carapace en cas de danger.', '20kg', '40cm', 'F', NULL, 8),
(8, 'Carabaffe', 'Quand on lui tapôte la tête, il se cache dans sa carapace, mais son corps ne peut pas y tenir en entier.', '80kg', '90cm', 'M', NULL, 9),
(9, 'Tortank', 'Une fois sa cible dans sa ligne de mire, il projette des jets d\'eau plus puissants qu\'une lance à incendie.', '200kg', '200cm', 'M', NULL, NULL),
(10, 'Chenipan', 'Lorsqu’il est attaqué par un Pokémon Vol, il utilise ses antennes pour dégager une odeur nauséabonde, mais cela le sauve rarement.', '5kg', '10cm', 'M', NULL, 11),
(11, 'Chrysacier', 'Sous sa carapace, il est tout mou et tendre. Il bouge très peu afin de ne pas secouer son corps encore fragile.', '10kg', '50cm', 'M', NULL, 12),
(12, 'Papilusion', 'Si l’on regarde ses grands yeux de plus près, on remarque qu’il s’agit en réalité d’une multitude de petits yeux réunis.', '12kg', '70cm', 'F', NULL, NULL),
(13, 'Aspicot', 'L\'aiguillon sur son front est très pointu. Il se cache dans les bois et les hautes herbes, où il se gave de feuilles.', '3.2kg', '30cm', 'M', NULL, 14),
(14, 'Coconfort', 'Il peut à peine bouger. Quand il est menacé, il sort parfois son aiguillon pour empoisonner ses ennemis.', '10kg', '60cm', 'M', NULL, 15),
(15, 'Dardagnan', 'Il se sert de ses trois aiguillons empoisonnés situés sur les pattes avant et l\'abdomen pour attaquer sans relâche ses adversaires.', '29.5kg', '100cm', 'F', NULL, NULL),
(16, 'Roucool', 'De nature très docile, il préfère projeter du sable pour se détendre plutôt que contre-attaquer.', '1.8kg', '30cm', 'M', NULL, 17),
(17, 'Roucoups', 'Ce Pokémon est très endurant. Il survole en permanence son territoire pour chasser.', '30kg', '110cm', 'F', NULL, 18),
(18, 'Roucarnage', 'Ce Pokémon vole à Mach 2 quand il chasse. Ses grandes serres sont des armes redoutables.', '39.5kg', '150cm', 'M', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pokemon_type`
--

DROP TABLE IF EXISTS `pokemon_type`;
CREATE TABLE IF NOT EXISTS `pokemon_type` (
  `pokemon_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`pokemon_id`,`type_id`),
  KEY `fk_pokemons_has_types_types1_idx` (`type_id`),
  KEY `fk_pokemons_has_types_pokemons1_idx` (`pokemon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pokemon_type`
--

INSERT INTO `pokemon_type` (`pokemon_id`, `type_id`) VALUES
(1, 9),
(13, 9),
(14, 9),
(15, 9),
(16, 10),
(17, 10),
(18, 10),
(1, 11),
(2, 11),
(3, 11),
(2, 12),
(3, 12),
(13, 12),
(14, 12),
(15, 12),
(2, 14),
(16, 18),
(17, 18),
(18, 18);

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `name`, `image`) VALUES
(1, 'Acier', NULL),
(2, 'Combat', NULL),
(3, 'Dragon', NULL),
(4, 'Eau', NULL),
(5, 'Electrik', NULL),
(6, 'Fée', NULL),
(7, 'Feu', NULL),
(8, 'Glace', NULL),
(9, 'Insecte', NULL),
(10, 'Normal', NULL),
(11, 'Plante', NULL),
(12, 'Poison', NULL),
(13, 'Psy', NULL),
(14, 'Roche', NULL),
(15, 'Sol', NULL),
(16, 'Spectre', NULL),
(17, 'Ténèbres', NULL),
(18, 'Vol', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(45) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `pseudo` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `user_type_id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ users_user_types_idx` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `lastname`, `firstname`, `pseudo`, `email`, `password`, `birthdate`, `gender`, `created_at`, `verified_at`, `user_type_id`, `level`) VALUES
(1, 'BURNS', 'Klein', 'Burn out', 'burnsklein3@gmail.com', '123456', '2005-01-01', 'M', '2021-02-01 08:11:16', '2021-01-02 08:11:16', 2, 18),
(2, 'CLEMENT', 'Mathias', 'MC Gregor', 'mathias.clement.web@gmail.com', '123456', '1987-01-01', 'M', '2020-10-21 12:19:00', '2021-01-02 08:11:16', 2, 33),
(3, 'DROLLET', 'Henry', 'Ghandi jr', 'nere.drollet@gmail.com', '123456', '2004-01-01', 'M', '2021-02-24 12:19:30', '2021-01-02 08:11:16', 2, 22),
(4, 'KONG', 'Brenda', 'SakiTahiti', 'brendakong26@gmail.com', '123456', '2002-01-01', 'F', '2019-10-13 23:11:16', '2021-01-02 08:11:16', 2, 23),
(5, 'KROMER', 'Maruata', 'Maru noa', 'maruata.kromer@yahoo.com', '123456', '1992-01-01', 'F', '2020-08-01 10:11:16', '2021-01-02 08:11:16', 2, 29),
(6, 'LEHARTEL', 'Matatini', 'Ginkgo', 'tiny.lehartel@gmail.com', '123456', '1989-01-01', 'M', '2020-12-24 05:11:16', '2021-01-02 08:11:16', 2, 31),
(7, 'LENOIR', 'Toariki', 'Diabetus', 'toariki2001@gmail.com', '123456', '2002-01-01', 'M', '2020-09-30 01:11:16', '2021-01-02 08:11:16', 2, 18),
(8, 'MAITUI', 'Mihiana', 'La binôme à maru', 'maituimihi@gmail.com', '123456', '1994-01-01', 'M', '2020-12-02 13:11:16', '2021-01-02 08:11:16', 2, 26),
(9, 'O\'CONNOR', 'Tehani', 'Séfiou', 'tehanioconnor@gmail.com', '123456', '2000-01-01', 'F', '2020-08-06 08:11:16', '2021-01-02 08:11:16', 2, 23),
(10, 'PARAMIO', 'Hereani', 'Madeleine', 'paramio.hereani1@gmail.com', '123456', '2002-01-01', 'M', '2020-10-22 08:11:16', '2021-01-02 08:11:16', 2, 19),
(11, 'RICHMOND ', 'Marie', 'Noelle', 'pofatu@gmail.com', '123456', '1979-01-01', 'F', '2020-02-26 06:11:16', '2021-01-02 08:11:16', 2, 41),
(12, 'RUAHE', 'Tamatoa', 'Burn out', 'tamatoa.ruahe@gmail.com', '123456', '2010-01-01', 'M', '2021-02-22 12:24:11', '2021-01-02 08:11:16', 2, 33),
(13, 'TAHA-LANDE', 'Erai', 'BackGground', 'landeerai@gmail.com', '123456', '2006-01-01', 'M', '2019-12-05 08:11:16', '2021-01-02 08:11:16', 2, 26),
(14, 'TEFAATAU', 'Tehauarii', 'Ze Emmerdeur ', 'tefaatau.tefaatau@icloud.com', '123456', '1998-01-01', 'M', '2020-04-20 08:11:16', '2021-01-02 08:11:16', 2, 23),
(15, 'WONG', 'Tamatoa', 'Cool987', 'wongmaraamu@gmail.com', '123456', '1988-01-01', 'F', '2020-03-01 08:11:16', '2021-01-02 08:11:16', 2, 32),
(16, 'MARO', 'Teremu', 'ElKing', '	teremu@1click.pf', '123456', '0001-01-01', 'M', '0001-01-01 00:00:01', '0001-01-01 00:00:01', 1, 100);

-- --------------------------------------------------------

--
-- Structure de la table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user_types`
--

INSERT INTO `user_types` (`id`, `name`) VALUES
(1, 'Administrateur'),
(2, 'Dresseur');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `abilities`
--
ALTER TABLE `abilities`
  ADD CONSTRAINT `fk_abilities_types` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `ability_pokemon`
--
ALTER TABLE `ability_pokemon`
  ADD CONSTRAINT `fk_pokemons_has_abilities_abilities1` FOREIGN KEY (`abilitie_id`) REFERENCES `abilities` (`id`),
  ADD CONSTRAINT `fk_pokemons_has_abilities_pokemons1` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemons` (`id`);

--
-- Contraintes pour la table `env_pokemon`
--
ALTER TABLE `env_pokemon`
  ADD CONSTRAINT `fk_pokemons_has_envs_envs1` FOREIGN KEY (`env_id`) REFERENCES `envs` (`id`),
  ADD CONSTRAINT `fk_pokemons_has_envs_pokemons1` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemons` (`id`);

--
-- Contraintes pour la table `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `fk_inventories_ users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_inventories_pokemons1` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemons` (`id`);

--
-- Contraintes pour la table `pokemons`
--
ALTER TABLE `pokemons`
  ADD CONSTRAINT `fk_pokemons_pokemons1` FOREIGN KEY (`pokemon_evo_id`) REFERENCES `pokemons` (`id`);

--
-- Contraintes pour la table `pokemon_type`
--
ALTER TABLE `pokemon_type`
  ADD CONSTRAINT `fk_pokemons_has_types_pokemons1` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemons` (`id`),
  ADD CONSTRAINT `fk_pokemons_has_types_types1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_ users_user_types` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
